﻿/*
 * 
 *      Author: Kennedy Tran
 *      Class - CSI - 335 - 51
 *      Assignment: RSS Poller
 *      Date Assigned: 2/13/2014
 *      Date Due: 11:59 PM on 2/27/2014
 *      Description: This program reads in an RSS link and writes to the form
 *                   or console the feed data from the channels.
 *      Certification of Authenticity:
 *          I certify that this assignment is my own work, with some help from
 *          Zygmyal Wangchuk, Paul Kubik, and Brandon Reynolds. 
 * 
 * *********************************************************************************/

#region Using
using RSS;
using System;
using System.Threading;
using System.Windows.Forms;
#endregion

namespace RSS_Form
{
    public partial class Form1 : Form
    {
        public static string URL;
        public static string DataString;

        public Form1()
        {
            DataString = string.Empty;
            this.InitializeComponent();
        }

        public void GoButton_Click(object sender, EventArgs e)
        {
            URL = this.URLTextBox.Text;
            this.URLTextBox.Text = "Loading RSS feeds...";
            TimerCallback callback = new TimerCallback(this.Poll);
            System.Threading.Timer pollTimer = new System.Threading.Timer(callback, null, 0, 15000);
        }

        public void Poll(object stateInfo)
        {
            RSSPoller poller = new RSSPoller();

            // Subscribe to the event in the newly made poller class so that when that event
            // is heard it will call the function Poller_RaiseCustomEvent.
            poller.RaiseCustomEvent += this.Poller_RaiseCustomEvent;
            poller.Poll(URL);
        }

        // This is the function that the event listener calls.
        void Poller_RaiseCustomEvent(object sender, PollEventArgs e)
        {
            // This calls the function that displays the channel that is recieved.
            this.DisplayText(e.GetChannel.GetDataString());
        }

        void DisplayText(string text)
        {
            string newText = text;

            // Used to avoid the ThreadSafe Exception.
            this.Invoke((MethodInvoker)delegate
            {
                this.FeedBox.Text = newText;
            });
        }
    }
}