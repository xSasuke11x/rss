﻿#region Using
using System;
using System.Collections.Generic;
#endregion

namespace RSS
{
    // Interface for the properties of the RSS.
    public interface IDataFeed
    {
        List<RSSItem> GetItems();

        List<string> GetProperties();

        List<string> GetPropertyNames();
    }

    // Interface for the Feed display.
    public interface IDataFeedDisplay
    {
        void FullDisplay();
    }

    // Another property for the Feed display.
    public interface IGetDataFeedDisplay
    {
        string GetDataString();
    }

    // This function loads in the data from the RSS.
    public class RSSFeedData : IDataFeed, IDataFeedDisplay, IGetDataFeedDisplay
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public List<RSSItem> Items = new List<RSSItem>();

        public string GetDataString()
        {
            string dataOutputString = string.Empty;

            dataOutputString += "Title: " + this.Title + "\nLink: " + this.Link + "\nDescription: " + this.Description + "\n\n";
            foreach (RSSItem item in this.Items)
            {
                dataOutputString += "\n\n";
                dataOutputString += item.GetTitle() + "\n";
                dataOutputString += item.GetLink() + "\n\n";
                if (item.GetDescription() != null)
                {
                    dataOutputString += item.GetDescription() + "\n";
                }
            }

            return dataOutputString;
        }

        // This function displays all of the Feed data.
        public void FullDisplay()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Title: " + this.Title);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("URL: " + this.Link);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Description: " + this.Description + "\n");

            foreach (RSSItem item in this.Items)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Title: " + item.GetTitle());
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("URL: " + item.GetLink() + "\n");
            }
        }

        // List that returns the items.
        public List<RSSItem> GetItems()
        {
            return this.Items;
        }

        // List that creates new properties in a list.
        public List<string> GetProperties()
        {
            return new List<string>() { this.Title, this.Link, this.Description };
        }

        // List that stores Title, Link, and Description in a list.
        public List<string> GetPropertyNames()
        {
            return new List<string>() { "Title", "Link", "Description" };
        }
    }
}