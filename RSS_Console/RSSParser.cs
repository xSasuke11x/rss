﻿#region Using
using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Syndication;
using System.Xml;
#endregion

namespace RSS
{
    public class RSSParser
    {
        private XmlReader reader;

        // Read 20 channels.
        public bool ValidateRSSFeed()
        {
            Rss20FeedFormatter validator = new Rss20FeedFormatter();
            return validator.CanRead(this.reader);
        }

        // Parse the RSS.
        public RSSFeedData ParseRSS(string feed)
        {
            this.reader = XmlReader.Create(new StringReader(feed));

            if (!this.ValidateRSSFeed())
            {
                throw new Exception("Invalid RSS feed");
            }

            this.reader.ReadToFollowing("channel");

            RSSFeedData channel = new RSSFeedData();
            channel.Items = new List<RSSItem>();

            bool header = true;

            // Displays Title, Link, Description, and Item.
            while (header)
            {
                this.reader.Read();
                if (this.reader.IsStartElement())
                {
                    switch (this.reader.Name)
                    {
                        case "title":
                            channel.Title = this.reader.ReadElementContentAsString();
                            break;

                        case "link":
                            channel.Link = this.reader.ReadElementContentAsString();
                            break;

                        case "description":
                            channel.Description = this.reader.ReadElementContentAsString();
                            break;

                        case "item":
                            header = false;
                            break;
                    }
                }
            }

            RSSItem item = new RSSItem();

            // Displays Title, Link, Description, and Item.
            while (this.reader.Read())
            {
                switch (this.reader.Name)
                {
                    case "title":
                        if (this.reader.IsStartElement())
                        {
                            item.Title = this.reader.ReadElementContentAsString();
                        }

                        break;

                    case "link":
                        if (this.reader.IsStartElement())
                        {
                            item.Link = this.reader.ReadElementContentAsString();
                        }

                        break;

                    case "description":
                        if (this.reader.IsStartElement())
                        {
                            item.Description = this.reader.ReadElementContentAsString();
                        }

                        break;

                    case "item":
                        if (!this.reader.IsStartElement())
                        {
                            channel.Items.Add(item);
                        }

                        break;
                }
            }

            return channel;
        }
    }
}