﻿#region Using
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
#endregion

namespace RSS
{
    //Interface for the Title, Link, and Description getters.
    public interface IGetItem
    {
        string GetTitle();

        string GetLink();

        string GetDescription();
    }

    // Interface for the Title, Link, and Description setters.
    public interface ISetItem
    {
        void SetTitle(string title);

        void SetLink(string link);

        void SetDescription(string description);
    }

    // Event for polling the RSS.
    public class PollEventArgs : EventArgs
    {
        // The event to send out.
        public PollEventArgs(RSSFeedData channelToPublish)
        {
            this.channel = channelToPublish;
        }

        RSSFeedData channel;

        public RSSFeedData GetChannel
        {
            // Used for the listeners to get the channel to display it.
            get { return this.channel; }
        }
    }

    public class RSSPoller
    {
        WebRequest request;
        HttpWebResponse response;

        public RSSPoller(string URL)
        {
            this._URL = URL;
        }

        public RSSPoller()
        {

        }

        // EventHandler for the Event.
        public event EventHandler<PollEventArgs> RaiseCustomEvent;

        public string _URL { get; set; }

        // Handler for the Poll Event.
        public void Poll(string URL)
        {
            RSSPoller poller = new RSSPoller();
            RSSFeedData channel = new RSSFeedData();

            Action<object> action = (object obj) =>
            {
                RSSParser parser = new RSSParser();

                string response = poller.GetResponse(URL);
                channel = parser.ParseRSS(response);
            };

            Task t1 = Task.Factory.StartNew(action, "alpha");
            try
            {
                t1.Wait();
            }
            catch (System.Exception ex)
            {
                throw new Exception(ex.Message);
            }

            this.OnRaiseCustomEvent(new PollEventArgs(channel));

            return;
        }

        // Custom EventHandler for Poll.
        protected virtual void OnRaiseCustomEvent(PollEventArgs e)
        {
            EventHandler<PollEventArgs> handler = this.RaiseCustomEvent;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        // Receive the response from the URL WebRequest.
        public string GetResponse(string URL)
        {
            try
            {
                this.request = WebRequest.Create(URL);
                this.response = (HttpWebResponse)this.request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            // Get the stream containing content returned by the server.
            Stream dataStream;

            try
            {
                dataStream = this.response.GetResponseStream();
            }
            catch (ProtocolViolationException ex)
            {
                throw new Exception(ex.Message);
            }

            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            // Read the content.
            string responseFromServer = reader.ReadToEnd();

            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            this.response.Close();

            return responseFromServer;
        }
    }

    // Getters and setters for the Items.
    public struct RSSItem : IGetItem
    {
        public string Title { get; set; }

        public string Link { get; set; }

        public string Description { get; set; }

        public string GetTitle()
        {
            return this.Title;
        }

        public string GetLink()
        {
            return this.Link;
        }

        public string GetDescription()
        {
            return this.Description;
        }
    }
}