﻿/*
 * 
 *      Author: Kennedy Tran
 *      Class - CSI - 335 - 51
 *      Assignment: RSS Poller
 *      Date Assigned: 2/13/2014
 *      Date Due: 11:59 PM on 2/27/2014
 *      Description: This program reads in an RSS link and writes to the form
 *                   or console the feed data from the channels.
 *      Certification of Authenticity:
 *          I certify that this assignment is my own work, with some help from
 *          Zygmyal Wangchuk, Paul Kubik, and Brandon Reynolds.
 * 
 * *********************************************************************************/

#region Using
using System;
using System.Collections.Generic;
#endregion

namespace RSS
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Start poller function.
            Poller poller = new Poller();
            poller.StartPoller();
        }
    }
}