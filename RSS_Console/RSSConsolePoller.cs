﻿#region Using
using System;
using System.Threading;
#endregion

namespace RSS
{
    public class Poller
    {
        private string URL;

        // Function that starts the poller.
        public void StartPoller()
        {
            Console.Write("Enter URL: ");
            this.URL = Console.ReadLine();

            // Create a Timer and CallBack function for the refresh.
            TimerCallback callback = new TimerCallback(this.Poll);
            Timer pollTimer = new Timer(callback, null, 0, 30000);

            string tempExit = Console.ReadLine();
            while (tempExit != "exit" && tempExit != "Exit")
            {
                tempExit = Console.ReadLine();
            }
        }

        // Use the RaiseCustomEvent function to call the Poll using the URL.
        private void Poll(object stateInfo)
        {
            RSSPoller poller = new RSSPoller();
            poller.RaiseCustomEvent += this.ConsolePoller_RaiseCustomEvent;
            poller.Poll(this.URL);
        }

        // Clears the console for the new feeds.
        void ConsolePoller_RaiseCustomEvent(object sender, PollEventArgs e)
        {
            Console.Clear();

            // Display the channel.
            e.GetChannel.FullDisplay();
        }
    }
}